#!/usr/bin/node
const child_process = require('child_process');
const fs = require('fs');

config = require("./config.sample.json");
config["default_server_config"]["m.homeserver"] = {
  "base_url": "https://neo.angry.im",
  "server_name": "neo.angry.im"
};
config["integrations_ui_url"] = "https://dimension.angry.im/element";
config["integrations_rest_url"] = "https://dimension.angry.im/api/v1/scalar";
config["integrations_widgets_urls"] = ["https://dimension.angry.im/widgets"];
config["room_directory"]["servers"] = [ "matrix.org", "neo.angry.im", "mozilla.modular.im", "feneas.org", "chat.privacytools.io" ];
config["enable_presence_by_hs_url"] = {
  "https://matrix.org": false,
  "https://matrix-client.matrix.org": false,
  "https://neo.angry.im": false
};
config["cross_origin_renderer_url"] = "https://riotcontent.angry.im/v1.html";
config["features"]["feature_custom_themes"] = "labs";
config["show_labs_settings"] = true;

// Themes
config["setting_defaults"]["custom_themes"] = [];

const theme_files = child_process.execSync("find element-themes -name '*.json'").toString().split("\n");

for (const file of theme_files) {
    if (file === "") continue;

    try {
        const data = JSON.parse(fs.readFileSync(file));
        config["setting_defaults"]["custom_themes"].push(data);
    } catch (err) {
        // Nothing
    }
}

// my custom themes
config["setting_defaults"]["custom_themes"].push(
      {
        "name": "Gruvbox Dark",
        "is_dark": true,
        "colors": {
          "accent-color": "#ebdbb2",
          "primary-color": "#ebdbb2",
          "warning-color": "#fb4934",
          "sidebar-color": "#3c3836",
          "roomlist-background-color": "#282828",
          "roomlist-text-color": "#ebdbb2",
          "roomlist-text-secondary-color": "#d5c4a1",
          "roomlist-highlights-color": "#665c54",
          "roomlist-separator-color": "#504945",
          "timeline-background-color": "#282828",
          "timeline-text-color": "#ebdbb2",
          "secondary-content": "#d5c4a1",
          "tertiary-content": "#bdae93",
          "timeline-text-secondary-color": "#d5c4a1",
          "timeline-highlights-color": "#665c54",
          "reaction-row-button-selected-bg-color": "#bdae93",
          "username-colors": [
            "#fb4934",
            "#b8bb26",
            "#fabd2f",
            "#83a598",
            "#d3869b",
            "#8ec07c",
            "#fe8019",
            "#ebdbb2"
          ],
          "avatar-background-colors": [
            "#d3869b",
            "#8ec07c",
            "#83a598"
          ]
        }
      }
);

delete config["piwik"]

console.log(JSON.stringify(config, null, 4))
