#!/usr/bin/env bash

/usr/sbin/tailscaled -state=mem: &
sleep 1
tailscale up --login-server=$TAILSCALE_SERVER --authkey=$TAILSCALE_KEY --hostname=$TAILSCALE_HOSTNAME &
node /app/server/server.js
