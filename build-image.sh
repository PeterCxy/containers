#!/bin/bash

set -e

[ -z "$1" ] && exit 1

DOCKER=docker

if ! command -v $DOCKER &> /dev/null; then
  DOCKER=podman
fi

$DOCKER build $1 -t gitea.angry.im/petercxy/$1:latest
$DOCKER push gitea.angry.im/petercxy/$1:latest

while ! [ "$?" -eq 0 ]; do
  $DOCKER push gitea.angry.im/petercxy/$1:latest
done
