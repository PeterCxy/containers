#!/bin/sh

touch /var/lib/headscale/db.sqlite
mkdir /var/lib/headscale/.cache
mkdir -p /var/run/headscale

caddy run --config /etc/Caddyfile &
exec headscale -c /etc/headscale/config.yaml serve
