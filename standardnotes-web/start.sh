#!/bin/sh

sed -i "s@https://app.standardnotes.com@${SN_WEB_PUBLIC_URL}@g" /public/index.html
sed -i "s@https://api.standardnotes.com@${SN_WEB_DEFAULT_SYNC_SERVER}@g" /public/index.html
sed -i "s@https://files.standardnotes.com@${SN_WEB_DEFAULT_FILES_HOST}@g" /public/index.html

exec static-web-server
